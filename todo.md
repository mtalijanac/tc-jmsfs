WORK IN PROGRESS
----------------

Features to implement
---------------------

## Associate queue to only one connection factory
All queues in configuration are global to all connection factories.
Enable queues linked only to a specific connection factory.

## Parameter replacement
Add support for parameters within message body which are populated on message send

## Scripting replacement
Add support for generating messages with embedded script


Implemented features
--------------------

## Examples folder
Virtual folder within root of JMSFS file system containing example configurations.

## String queues in connection.xml
Add resolving queues in configuration from string values.

## Reload configuration on change
Spring configurations are never refreshed. If they change they should be reloaded.

## Clear q
Delete on a Q will clear content of that Q

## Edit/update configuration with F4
Edit connection.xml by pressing F4.

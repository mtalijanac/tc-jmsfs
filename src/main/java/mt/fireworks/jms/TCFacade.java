package mt.fireworks.jms;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import lombok.extern.log4j.Log4j;
import mt.fireworks.jms.ConfigTree.MessageNode;
import mt.fireworks.jms.ConfigTree.Node;
import mt.fireworks.jms.ConfigTree.QueueNode;
import plugins.wfx.RemoteInfo;
import plugins.wfx.WFXPluginAdapter;
import plugins.wfx.Win32FindData;

/**
 * Facade for Total Commander.
 */

@Log4j
public class TCFacade extends WFXPluginAdapter {
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String fsGetDefRootName(final int maxlen) {
		log.debug("TCFacade.fsGetDefRooName() - JMS_FS");
		return "JMS_FS";
	}


    /**
     * Close all CFs/spring context while unloading plugin.
     *
     * {@inheritDoc}
     */
    public void fsContentPluginUnloading() {
        log.debug("TCFacade.fsContentPluginUnloading()");
        ConfigTree ct = ConfigTree.instance();
        Node root = ct.getRoot();
        root.close();
    }

    @Override
    public Object fsFindFirst(final String pathPar, final Win32FindData findData) {
        ConfigTree ct = ConfigTree.instance();
        Node node = ct.select(pathPar);

        if (node == null) {
            log.debug("TCFacade.fsFindFirst() can't find node for path: '" + pathPar + "'");
            long lastErrorMessage = Win32FindData.ERROR_NO_MORE_FILES;
            findData.setLastErrorMessage(lastErrorMessage);
            return INVALID_HANDLE_VALUE;
        }

        log.debug("TCFacade.fsFindFirst() path: '" + pathPar + "', node: '" + node.getClass().getSimpleName() + ":" + node.getName() + "'");

        ArrayList<Node> children = node.refreshChildren();
        if (children == null || children.isEmpty()) {
            long lastErrorMessage = Win32FindData.ERROR_NO_MORE_FILES;
            findData.setLastErrorMessage(lastErrorMessage);
            return null;
        }

        Iterator<Node> iterator = children.iterator();
        Node child = iterator.next();
        child.findData(findData);
        return iterator;
    }

    @Override
    public boolean fsFindNext(Object handle, Win32FindData findData) {
        if (handle == null) {
            log.debug("TCFacade.fsFindNext() handle is null");
            return false;
        }

        if (!(handle instanceof Iterator)) {
            log.debug("TCFacade.fsFindNext() unreasonable handle: " + handle.toString());
            return false;
        }

        @SuppressWarnings("unchecked")
		Iterator<Node> iter = (Iterator<ConfigTree.Node>) handle;
        if (!iter.hasNext()) {
            log.debug("TCFacade.fsFindNext()");
            return false;
        }

        Node child = iter.next();
        child.findData(findData);
        log.debug("TCFacade.fsFindNext() found: '" + child.fullpath() + "'");
        return true;
    }

    @Override
    public int fsFindClose(Object handle) {
        log.debug("TCFacade.fsFindClose()");
        return 0;
    }


    /*
        FsPutFile is called to transfer a file from the normal file system (drive letters or UNC) to the plugin's file system.
    */
    @Override
    public int fsPutFile(String localName, String remoteName, int copyFlags) {
        if (log.isDebugEnabled()) {
            log.debug("JMSBrokers.fsPutFile() remoteName='" + remoteName
                    + "', localName='" + localName + "', copyFlags='" + copyFlags + "'");
        }

        try {
            File localFile = new File(localName);
            if (localFile.isDirectory()) {
                log.debug("Can't send directory");
                return FS_EXEC_ERROR;
            }


            boolean overwrite = (copyFlags & FS_COPYFLAGS_OVERWRITE) != 0;
            boolean resume = (copyFlags & FS_COPYFLAGS_RESUME) != 0;
            boolean move = (copyFlags & FS_COPYFLAGS_MOVE) != 0;
            if (resume) {
                log.debug("Resume not supported");
                return FS_FILE_NOTSUPPORTED;
            }

            String path = FilenameUtils.getPath(remoteName);

            ConfigTree ct = ConfigTree.instance();
            Node node = ct.select(path);
            if (node == null) {
                log.debug("Can't find node for path: '" + path + "'.");
                return 0;
            }

            if (!(node instanceof QueueNode)) {
                log.debug("Expected QueueNode at path: '" + remoteName + "'. Found: '" + node.getClass().getName() + "'.");
                return 0;
            }

            QueueNode qnode = (QueueNode) node;


            boolean isTextFile = FilenameUtils.isExtension(localName, new String[] {"txt", "text", "TXT"});

            if (isTextFile) {
                String content = FileUtils.readFileToString(localFile, StandardCharsets.UTF_8);
                qnode.sendTextMessage(content);
                log.debug("Sent text message: '" + content.length() + "' chars\n" + content);
            }
            else {
                byte[] content = FileUtils.readFileToByteArray(localFile);
                qnode.sendBytesMessage(content);
                log.debug("Sent bytesmessage: '"  + content.length + "' bytes");
            }

            if (move) {
                localFile.delete();
            }

            return FS_FILE_OK;
        }
        catch (RuntimeException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }
        catch (IOException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }
    }


    /**
     * FsGetFile is called to transfer a file from the plugin's file system to the normal file system (drive letters or UNC).
     */
    @Override
    public int fsGetFile(String remoteName, String localName, int copyFlags, RemoteInfo remoteInfo) {
        if (log.isDebugEnabled()) {
            log.debug("TCFacade.fsGetFile() remoteName=" + remoteName + ", localName=" + localName + ", copyFlags=" + copyFlags 
            		  + ", remoteInfo.getSizeLow()=" + remoteInfo.getSizeLow() 
                      + ", remoteInfo.getSizeHigh()="+ remoteInfo.getSizeHigh()
                      + ", remoteInfo.getLastWriteTime()="+ remoteInfo.getLastWriteTime().getDate()
                      + ", remoteInfo.getAttr()=" + remoteInfo.getAttr());
        }


        try {
            boolean overwrite = (copyFlags & FS_COPYFLAGS_OVERWRITE) != 0;
            boolean resume = (copyFlags & FS_COPYFLAGS_RESUME) != 0;
            boolean move = (copyFlags & FS_COPYFLAGS_MOVE) != 0;
            if (resume) {
                return FS_FILE_NOTSUPPORTED;
            }

            ConfigTree ct = ConfigTree.instance();
            Node node = ct.select(remoteName);
            if (node == null) {
                log.debug("Can't find node for path: '" + remoteName + "'.");
                return 0;
            }

            if (!(node instanceof MessageNode)) {
                log.debug("Expected MessageNode at path: '" + remoteName + "'. Found: '" + node.getClass().getName() + "'.");
                return 0;
            }

            MessageNode msgNode = (MessageNode) node;
            Object payload = move ? msgNode.receiveMessage()
                                  : msgNode.browseMessage();

            if (payload == null) {
                log.debug("No payload received from message");
                return FS_FILE_READERROR;
            }

            File localFile = new File(localName);
            if (!localFile.getParentFile().canWrite()) {
                log.debug("Can't write to folder: '" + localFile.getParentFile().getAbsolutePath() + "'");
                return FS_FILE_WRITEERROR;
            }

            if (localFile.exists()) {
                if (!overwrite) {
                    log.debug("Owerwrite disabled. File: '" + localName + "'.");
                    return FS_FILE_EXISTS;
                }
                if (!localFile.canWrite() ) {
                    log.debug("Can't write over file: '" + localName + "'.");
                    return FS_FILE_WRITEERROR;
                }
                localFile.delete();
                log.debug("Deleted file: '" + localFile.getAbsolutePath() + "'.");
            }


            try {
                if (payload instanceof String) {
                    FileUtils.writeStringToFile(localFile, (String) payload);
                    return FS_FILE_OK;
                }
                else if (payload instanceof byte[]) {
                    FileUtils.writeByteArrayToFile(localFile, (byte[]) payload);
                    return FS_FILE_OK;
                }
            }
            catch (IOException e) {
                log.debug("Error writing to file '" + localName + "'.", e);
                return FS_FILE_WRITEERROR;
            }

        }
        catch (RuntimeException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }

        return 0;
    }
    
    /**
	 * FsDeleteFile is called to delete a file from the plugin's file system.
	 * 
	 * @param remoteName
	 *            RemoteName Name of the file to be deleted, with full path. The
	 *            name always starts with a backslash, then the names returned
	 *            by FsFindFirst/FsFindNext separated by backslashes.
	 * @return Return TRUE if the file could be deleted, FALSE if not.
	 * 
	 * {@inheritDoc}
	 */
	public boolean fsDeleteFile(final String remoteName) {
		if (log.isDebugEnabled()) {
			log.debug("TCFacade.fsDeleteFile() removeName: '" + remoteName + "'");
		}
		
		try {
			ConfigTree ct = ConfigTree.instance();
	        Node node = ct.select(remoteName);
	        if (node == null) {
	            log.debug("Can't find node for path: '" + remoteName + "'.");
	            return false;
	        }
	
	        if (!(node instanceof MessageNode)) {
	            log.debug("Expected MessageNode at path: '" + remoteName + "'. Found: '" + node.getClass().getName() + "'.");
	            return false;
	        }
	
	        MessageNode msgNode = (MessageNode) node;
	        Object payload = msgNode.receiveMessage();
	        return true;
		}
		catch (RuntimeException exc) {
			log.error(exc.getMessage(), exc);
			return false;
		}
	}

}

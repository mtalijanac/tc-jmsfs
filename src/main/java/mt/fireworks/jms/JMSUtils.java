package mt.fireworks.jms;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.JmsDestinationAccessor;

import lombok.extern.log4j.Log4j;

@Log4j
public class JMSUtils {

    GenericXmlApplicationContext loadXmlContext(File connectionXml) {
        FileSystemResource contextResource = new FileSystemResource( connectionXml );
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load(contextResource);
        ctx.refresh();
        return ctx;
    }

    BrowserCallback<List<Message>> listMessages = new BrowserCallback<List<Message>>() {
        public List<Message> doInJms(Session session, QueueBrowser browser) throws JMSException {
            ArrayList<Message> msgs = new ArrayList<Message>();

            Enumeration enumeration = browser.getEnumeration();
            while (enumeration.hasMoreElements()) {
                Message msg = (Message) enumeration.nextElement();
                msgs.add(msg);
            }

            return msgs;
        }
    };

    List<Message> browseMessages(Queue destination, JmsTemplate jt) {
        List<Message> messages = jt.browse(destination, listMessages);
        return messages;
    }

    List<Message> browseMessages(String destination, JmsTemplate jt) {
        List<Message> messages = jt.browse(destination, listMessages);
        return messages;
    }

    JmsTemplate newJT(ConnectionFactory cf) {
        JmsTemplate jt = new JmsTemplate(cf);
        // jt.setReceiveTimeout(1000);
        jt.afterPropertiesSet();
        return jt;
    }

    Map<String, Object> header(Message message) {
        try {
            Map<String, Object> result = new HashMap<>();
            Enumeration propertyNames = message.getPropertyNames();
            while (propertyNames.hasMoreElements()) {
                String name = (String) propertyNames.nextElement();
                Object val = message.getObjectProperty(name);
                result.put(name, val);
            }
            return result;
        }
        catch (JMSException e) {
            log.error("Error reading property values from JMS message", e);
            return null;
        }
    }

    Object payload(Message message) {
        if (message instanceof TextMessage) {
            TextMessage tmsg = (TextMessage) message;
            try {
                String payload = tmsg.getText();
                return payload;
            }
            catch (JMSException e) {
                log.error("Error reading text from TextMessage", e);
                return null;
            }
        }

        if (message instanceof BytesMessage) {
            try {
                BytesMessage bmsg = (BytesMessage) message;
                int bodyLength = (int) bmsg.getBodyLength();
                byte[] data = new byte[bodyLength];
                bmsg.readBytes(data);
                return data;
            }
            catch (JMSException e) {
                log.error("Error reading data from BytesMessage", e);
                return null;
            }
        }

        log.info("Message type not supported: '" + message.getClass().getSimpleName() + "'.");
        return null;
    }

    String filename(Message message) {
        try {
            String extension = "";
            if (message instanceof TextMessage) {
                extension = ".text";
            }
            else if (message instanceof BytesMessage) {
                extension = ".bytes";
            }
            else if (message instanceof MapMessage) {
                extension = ".map";
            }
            else if (message instanceof ObjectMessage) {
                extension = ".object";
            }
            else if (message instanceof StreamMessage) {
                extension = ".stream";
            }
            String jmsMessageID = message.getJMSMessageID();
            String filename = jmsMessageID + extension;
            return filename;
        }
        catch (JMSException e) {
            log.error("Error reading data from message.", e);
        }
        return "error";
    }
}

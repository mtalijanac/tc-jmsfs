package mt.fireworks.jms;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import lombok.Getter;
import lombok.extern.log4j.Log4j;
import plugins.FileTime;
import plugins.wfx.Win32FindData;

@Log4j
public class ConfigTree {
    static JMSUtils jmsutils = new JMSUtils();


    static ConfigTree ctInstance;

    public static ConfigTree instance() {
        if (ctInstance == null) {
            ctInstance = new ConfigTree();
            ctInstance.root = new RootNode();
        }
        return ctInstance;
    }



    @Getter Node root;

    public interface Node {
        String getName();
        String fullpath();

        Node getParent();
        ArrayList<Node> refreshChildren();

        default void close() {
        }

        default long getAttributes() {
            return 0L;
        }

        default void findData(Win32FindData data) {
            data.setFileName(getName());
            data.setFileAttributes(getAttributes());

            Long messageSize = getMessageSize();
             if (messageSize != null) {
                 data.setFileSizeHigh(0);
                 data.setFileSizeLow(messageSize);
             }

             Long creationTime = getCreationTime();
             if (creationTime != null) {
                 FileTime ft = new FileTime();
                 ft.setDate(creationTime);
                 data.setCreationTime(ft);
             }
        }

        default Long getCreationTime() {
            return null;
        }

        default Long getMessageSize() {
            return null;
        }

    }

    public static abstract class BaseNode implements Node {
        @Getter String name;
        @Getter Node parent;
        @Getter long attributes;

        public String toString() {
            return this.getClass().getSimpleName() + ":" + name;
        }

        public String fullpath() {
            String fp = ConfigTree.instance().fullpath(this);
            return fp;
        }
    }


    public static class RootNode extends BaseNode {
    	ArrayList<Node> children;
    	
        public RootNode() {
            name = "\\";
            parent = this;
            attributes = Win32FindData.FILE_ATTRIBUTE_DIRECTORY;
            children = new ArrayList<ConfigTree.Node>();
        }

        @Override
        public void close() {
            if (children == null || children.isEmpty()) {
                return;
            }
            for (Node n: children) {
                n.close();
            }
            children.clear();
        }


        public ArrayList<Node> refreshChildren() {
            File workingDir = workingDir();
            ArrayList<String> foundConfigPaths = listXmlPaths(workingDir);
            ArrayList<String> childXmlPaths = childXmlPaths();

            //
            // Close and remove child contexts
            //
            ArrayList<String> removedXml = new ArrayList<>(childXmlPaths);
            removedXml.removeAll(foundConfigPaths);

            if (!removedXml.isEmpty()) {
                ArrayList<Node> forRemoval = new ArrayList<>();
                for (Node n: children) {
                    ContextNode childNode = (ContextNode) n;
                    String childPath = childNode.getContextFile().getAbsolutePath();
                    if (!removedXml.contains(childPath)) {
                        continue;
                    }
                    forRemoval.add(n);
                    GenericXmlApplicationContext context = childNode.getContext();
                    if (context != null) {
                        context.close();
                    }
                    log.debug("Closed ContextNode: '" + childPath + "'.");
                }
                children.removeAll(forRemoval);
            }


            //
            // Add new configuration
            //
            ArrayList<String> newFiles = new ArrayList<>(foundConfigPaths);
            newFiles.removeAll(childXmlPaths);

            if (!newFiles.isEmpty()) {
                for (String newFile: newFiles) {
                    ContextNode newNode = new ContextNode(this, new File(newFile));
                    children.add(newNode);
                    log.debug("Added ContextNode for file: '" + newFile + "'.");
                }
            }

            return children;
        }

        File workingDir() {
            File workingDir = new File(".");
            try {
                workingDir = workingDir.getCanonicalFile();
            }
            catch (IOException e) {
                log.error("Unexpected problem calculation canonicalFile", e);
            }
            log.debug("Working dir: '" + workingDir.getAbsolutePath() + "'");
            return workingDir;
        }

        ArrayList<String> listXmlPaths(File workingDir) {
            IOFileFilter xmlFilter = FileFilterUtils.suffixFileFilter("xml", IOCase.INSENSITIVE);
            Collection<File> foundXmlFiles = FileUtils.listFiles(workingDir, xmlFilter, null);
            ArrayList<String> foundXmlPaths = new ArrayList<>();
            for (File f: foundXmlFiles) {
                String path = f.getAbsolutePath();
                foundXmlPaths.add(path);
                log.debug("Found xml file: '" + path + "'.");
            }
            return foundXmlPaths;
        }

        ArrayList<String> childXmlPaths() {
            ArrayList<String> childConfigPaths = new ArrayList<>();
            for (Node n: children) {
                ContextNode childNode = (ContextNode) n;
                String childPath = childNode.getContextFile().getAbsolutePath();
                childConfigPaths.add(childPath);
                log.debug("Child file: '" + childPath + "'.");
            }
            return childConfigPaths;
        }

    }


    public static class ContextNode extends BaseNode {
        @Getter File contextFile;
        @Getter long lastModified;
        @Getter GenericXmlApplicationContext context;

        public ContextNode(Node father, File ctxFile) {
            this.parent = father;
            this.contextFile = ctxFile;
            this.lastModified = ctxFile.lastModified();
            this.attributes = Win32FindData.FILE_ATTRIBUTE_DIRECTORY;
        }

        @Override
        public void close() {
            if (context == null) {
                return;
            }
            log.info("Cloasing context at: '" + this.getClass().getSimpleName() + ":" + this.getName() + "'");
            context.close();
            context = null;
        }

        public String getName() {
            return contextFile.getName();
        }

        @Override
        public java.util.ArrayList<Node> refreshChildren() {
            if (context == null) {
                this.lastModified = contextFile.lastModified();
                context = jmsutils.loadXmlContext(contextFile);
            }

            if (context != null) {
                long modified = contextFile.lastModified();
                if (modified != this.lastModified) {
                    this.close();
                    this.lastModified = modified;
                    context = jmsutils.loadXmlContext(contextFile);
                }
            }

            ArrayList<Node> result = new ArrayList<ConfigTree.Node>();

            Map<String, ConnectionFactory> cfs = context.getBeansOfType(ConnectionFactory.class);
            for (Entry e: cfs.entrySet()) {
                String id = (String) e.getKey();
                ConnectionFactory cf = (ConnectionFactory) e.getValue();
                CFNode cfNode = new CFNode(this, id, cf);
                result.add(cfNode);
            }

            return result;
        }
    }



    @Getter
    public static class CFNode extends BaseNode {
        ConnectionFactory connectionFactory;
        JmsTemplate jmsTemplate;

        public CFNode(Node father, String name, ConnectionFactory cf) {
            this.parent = father;
            this.connectionFactory = cf;
            this.jmsTemplate = jmsutils.newJT(cf);
            this.name = name;
            this.attributes = Win32FindData.FILE_ATTRIBUTE_DIRECTORY;
        }

        @Override
        public ArrayList<Node> refreshChildren() {
            Node ctxNode = getParent();
            ContextNode configNode = (ContextNode) ctxNode;
            GenericXmlApplicationContext context = configNode.getContext();

            ArrayList<Node> result = new ArrayList<ConfigTree.Node>();

            Map<String, Queue> cfs = context.getBeansOfType(Queue.class);
            for (Entry e: cfs.entrySet()) {
                String id = (String) e.getKey();
                Queue q = (Queue) e.getValue();
                QueueNode qNode = new QueueNode(this, id, q);
                result.add(qNode);
            }

            return result;
        }
    }

    @Getter
    public static class QueueNode extends BaseNode {
        Queue queue;

        public QueueNode(Node father, String beanId, Queue q) {
            this.parent = father;
            this.name = beanId;
            this.queue = q;
            this.attributes = Win32FindData.FILE_ATTRIBUTE_DIRECTORY;
        }

        public ArrayList<Node> refreshChildren() {
            CFNode cfnode = (CFNode) getParent();
            JmsTemplate jt = cfnode.getJmsTemplate();
            List<Message> messages = jmsutils.browseMessages(queue, jt);

            ArrayList<Node> result = new ArrayList<ConfigTree.Node>();
            for (Message msg: messages) {
                try {
                    String filename = jmsutils.filename(msg);
                    String jmsMessageID = msg.getJMSMessageID();
                    long jmsTimestamp = msg.getJMSTimestamp();
                    Object payload = jmsutils.payload(msg);
                    long msgsize = 0;
                    if (payload instanceof String) {
                        String text = (String) payload;
                        msgsize = text.length();
                    }
                    else if (payload instanceof byte[]) {
                        byte[] data = (byte[]) payload;
                        msgsize = data.length;
                    }

                    MessageNode messageNode = new MessageNode(this, filename, jmsMessageID, jmsTimestamp, msgsize);
                    result.add(messageNode);
                }
                catch (JMSException e) {
                    throw new RuntimeException(e);
                }
            }

            return result;
        }

        JmsTemplate getJmsTemplate() {
            CFNode cfn = (CFNode) this.getParent();
            JmsTemplate jmsTemplate = cfn.getJmsTemplate();
            return jmsTemplate;
        }

        public void sendTextMessage(String payload) {
            JmsTemplate jmsTemplate = getJmsTemplate();
            jmsTemplate.send(this.queue, new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    TextMessage msg = session.createTextMessage(payload);
                    return msg;
                }
            });
        }

        public void sendBytesMessage(byte[] payload) {
            JmsTemplate jmsTemplate = getJmsTemplate();
            jmsTemplate.send(this.queue, new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    BytesMessage msg = session.createBytesMessage();
                    msg.writeBytes(payload);
                    return msg;
                }
            });
        }
    }

    @Getter
    public static class MessageNode extends BaseNode {
        String JMSMessageID;
        Long creationTime;
        Long messageSize;
        String type;

        public MessageNode(Node father, String filename, String msgId, long creationTime, long msgSize) {
            this.parent = father;
            this.name = filename;
            this.JMSMessageID = msgId;
            this.attributes = Win32FindData.FILE_ATTRIBUTE_NORMAL;
            this.creationTime = creationTime;
            this.messageSize = msgSize;
        }

        @Override
        public ArrayList<Node> refreshChildren() {
            return null;
        }


        JmsTemplate getJmsTemplate() {
            QueueNode qn = (QueueNode) parent;
            CFNode cfn = (CFNode) qn.getParent();
            JmsTemplate jmsTemplate = cfn.getJmsTemplate();
            return jmsTemplate;
        }

        String destinationName() {
            QueueNode qn = (QueueNode) parent;
            return qn.getName();
        }

        String messageSelector() {
            String messageSelector = "JMSMessageID='" + JMSMessageID + "'";
            return messageSelector;
        }

        public Object receiveMessage() {
            JmsTemplate jmsTemplate = getJmsTemplate();
            String destinationName = destinationName();
            String messageSelector = messageSelector();

            log.debug("Receiving message with selector: '" + messageSelector + "'.");
            Message message = jmsTemplate.receiveSelected(destinationName, messageSelector);
            if (message == null) {
                log.debug("No message found for JMSMessageID: '" + JMSMessageID + "'");
                return null;
            }

            Object payload = jmsutils.payload(message);
            return payload;
        }

        public Object browseMessage() {
            JmsTemplate jmsTemplate = getJmsTemplate();
            String destinationName = destinationName();
            String messageSelector = messageSelector();

            log.debug("Browsing message with selector: '" + messageSelector + "'.");

            Object payload = jmsTemplate.browseSelected(destinationName, messageSelector, new BrowserCallback<Object>() {
                public Object doInJms(Session session, QueueBrowser browser) throws JMSException {
                    @SuppressWarnings("unchecked")
					Enumeration<Message> enumeration = browser.getEnumeration();
                    if (!enumeration.hasMoreElements()) {
                        return null;
                    }
                    Message message = enumeration.nextElement();
                    Object payload = jmsutils.payload(message);
                    return payload;
                }
            });
            return payload;
        }
    }


    public String fullpath(Node node) {
        if (node.getParent() == null) {
            return node.getName();
        }

        if (node.getParent() == node) {
            return node.getName();
        }

        return fullpath(node.getParent()) + "\\" + node.getName();
    }

    public Node nextSibling(Node node) {
        Node nodeParent = node.getParent();
        if (nodeParent == null) {
            return null;
        }

        ArrayList<Node> siblings = nodeParent.refreshChildren();
        if (siblings == null) {
            return null;
        }

        int index = siblings.indexOf(node);
        if (index + 1 >= siblings.size()) {
            return null;
        }

        Node nextSibling = siblings.get(index + 1);
        return nextSibling;
    }


    public Node select(String fullpath) {
        String[] split = StringUtils.split(fullpath, "\\");
        Node node = root;
        for (String name: split) {
            if (StringUtils.isEmpty(name)) {
                continue;
            }

            Node child = node.refreshChildren().stream()
                .filter(it -> name.equalsIgnoreCase(it.getName()))
                .findFirst()
                .orElse(null);

            if (child == null) {
            	String tree = printTree();
            	log.debug("Searching for '" + fullpath + "':\n" + tree);
            	return null;
            }

            node = child;
        }
        return node;
    }
    
    String printTree() {
    	ConfigTree instance = ConfigTree.instance();
    	Node root = instance.getRoot();
    	StringBuilder sb = new StringBuilder();
    	printNode(root, sb, 0);
    	return sb.toString();
    }
    
    void printNode(Node node, StringBuilder sb, int times) {
    	if (node == null) {
    		return;
    	}
    	for (int i = 0; i < times; i++) {
    		sb.append("  ");
    	}
    	String name = node.getName();
    	sb.append(name).append("\n");
    	
    	ArrayList<Node> children = node.refreshChildren();
    	if (children == null || children.isEmpty()) {
    		return;
    	}
    	
    	for (Node ch: children) {
    		printNode(ch, sb, times + 1);
    	}
    }

}

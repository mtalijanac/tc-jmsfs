package mt.fireworks.jms2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import lombok.extern.log4j.Log4j;
import mt.fireworks.jms2.PathOperations.Consts;
import plugins.wfx.RemoteInfo;
import plugins.wfx.WFXPluginAdapter;
import plugins.wfx.WFXPluginInterface;
import plugins.wfx.Win32FindData;

@Log4j
public class TCFacade2 extends WFXPluginAdapter {

    final PathOperations ops = new PathOperations();

    /** {@inheritDoc} */
    public String fsGetDefRootName(final int maxlen) {
        log.debug("TCFacade.fsGetDefRooName() - JMS_FS2");
        return "JMS_FS2";
    }


    /** {@inheritDoc} */
    public void fsContentPluginUnloading() {
        log.debug("TCFacade.fsContentPluginUnloading() - closing all open resources");
        ops.unload();
    }


    /** {@inheritDoc} */
    public Object fsFindFirst(final String pathPar, final Win32FindData findData) {
        log.debug("fsFindFirst params: pathPar: '" + pathPar + "'");
        ArrayList<Consumer<Win32FindData>> children = null;

        try {
            children = ops.listPath(pathPar);
        }
        catch (IOException e) {
            log.error("IOException occured", e);
            return INVALID_HANDLE_VALUE;
        }

        if (children == null || children.isEmpty()) {
            log.debug("TCFacade.fsFindFirst() can't find node for path: '" + pathPar + "'");
            long lastErrorMessage = Win32FindData.ERROR_NO_MORE_FILES;
            findData.setLastErrorMessage(lastErrorMessage);
            return null;
        }

        Iterator<Consumer<Win32FindData>> iterator = children.iterator();
        Consumer<Win32FindData> consumer = iterator.next();
        consumer.accept(findData);
        return iterator;
    }


    /** {@inheritDoc} */
    public boolean fsFindNext(Object handle, Win32FindData findData) {
        if (handle == null) {
            log.debug("TCFacade.fsFindNext() handle is null");
            return false;
        }

        if (!(handle instanceof Iterator)) {
            log.debug("TCFacade.fsFindNext() unreasonable handle: " + handle.toString());
            return false;
        }

        Iterator<Consumer<Win32FindData>> iter = (Iterator<Consumer<Win32FindData>>) handle;
        if (!iter.hasNext()) {
            return false;
        }

        Consumer<Win32FindData> consumer = iter.next();
        consumer.accept(findData);
        return true;
    }


    /** {@inheritDoc} */
    public int fsFindClose(Object handle) {
        log.debug("TCFacade.fsFindClose()");
        return 0;
    }


    /*
        FsPutFile is called to transfer a file from the normal file system (drive letters or UNC) to the plugin's file system.
    */
    @Override
    public int fsPutFile(String localName, String remoteName, int copyFlags) {
        if (log.isDebugEnabled()) {
            log.debug("JMSBrokers.fsPutFile() remoteName='" + remoteName
                    + "', localName='" + localName + "', copyFlags='" + copyFlags + "'");
        }

        try {
            File localFile = new File(localName);
            if (localFile.isDirectory()) {
                log.debug("Can't send directory: '" + localName + "'");
                return FS_EXEC_ERROR;
            }

            String[] split = StringUtils.split(remoteName, "\\");

            if (split.length == 1) {
                return ops.copyConfiguraton(localName, remoteName);
            }

            if (split.length != Consts.idxMsg + 1) {
                log.debug("Can't send message to destination: '" + remoteName + "'");
                return FS_EXEC_ERROR;
            }

            boolean overwrite = (copyFlags & FS_COPYFLAGS_OVERWRITE) != 0;
            boolean resume = (copyFlags & FS_COPYFLAGS_RESUME) != 0;
            boolean move = (copyFlags & FS_COPYFLAGS_MOVE) != 0;
            if (resume) {
                log.debug("Resume not supported");
                return FS_FILE_NOTSUPPORTED;
            }

            log.debug("Copying file: '" + localName + "' to: '" + remoteName + "'.");
            ops.copyLocalFileToQ(localName, remoteName);
            if (move) {
                log.debug("Deleting file: '" + localName + "'");
                localFile.delete();
            }

            return FS_FILE_OK;
        }
        catch (RuntimeException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }
        catch (IOException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }
    }


    /**
     * FsGetFile is called to transfer a file from the plugin's file system to the normal file system (drive letters or UNC).
     */
    @Override
    public int fsGetFile(String remoteName, String localName, int copyFlags, RemoteInfo remoteInfo) {
        if (log.isDebugEnabled()) {
            log.debug("TCFacade.fsGetFile() remoteName=" + remoteName + ", localName=" + localName + ", copyFlags=" + copyFlags
                      + ", remoteInfo.getSizeLow()=" + remoteInfo.getSizeLow()
                      + ", remoteInfo.getSizeHigh()="+ remoteInfo.getSizeHigh()
                      + ", remoteInfo.getLastWriteTime()="+ remoteInfo.getLastWriteTime().getDate()
                      + ", remoteInfo.getAttr()=" + remoteInfo.getAttr());
        }

        try {
            boolean overwrite = (copyFlags & FS_COPYFLAGS_OVERWRITE) != 0;
            boolean resume = (copyFlags & FS_COPYFLAGS_RESUME) != 0;
            boolean move = (copyFlags & FS_COPYFLAGS_MOVE) != 0;
            if (resume) {
                return FS_FILE_NOTSUPPORTED;
            }

            File localFile = new File(localName);
            if (!localFile.getParentFile().canWrite()) {
                log.debug("Can't write to folder: '" + localFile.getParentFile().getAbsolutePath() + "'");
                return FS_FILE_WRITEERROR;
            }

            if (localFile.exists()) {
                if (!overwrite) {
                    log.debug("Owerwrite disabled. File: '" + localName + "'.");
                    return FS_FILE_EXISTS;
                }
                if (!localFile.canWrite() ) {
                    log.debug("Can't write over file: '" + localName + "'.");
                    return FS_FILE_WRITEERROR;
                }
                localFile.delete();
                log.debug("Deleted file: '" + localFile.getAbsolutePath() + "'.");
            }


            Object payload = null;

            if (move) {
                payload = ops.readMessagePayload(remoteName);
            }
            else {
                payload = ops.browsePath(remoteName);
            }


            if (payload == null) {
                log.debug("No payload received from message");
                return FS_FILE_READERROR;
            }

            try {
                if (payload instanceof String) {
                    FileUtils.writeStringToFile(localFile, (String) payload);
                    return FS_FILE_OK;
                }
                else if (payload instanceof byte[]) {
                    FileUtils.writeByteArrayToFile(localFile, (byte[]) payload);
                    return FS_FILE_OK;
                }
            }
            catch (IOException e) {
                log.debug("Error writing to file '" + localName + "'.", e);
                return FS_FILE_WRITEERROR;
            }

        }
        catch (RuntimeException | IOException e) {
            log.error(e.getMessage(), e);
            return FS_FILE_NOTSUPPORTED;
        }

        return 0;
    }

    /**
     * FsDeleteFile is called to delete a file from the plugin's file system.
     *
     * @param remoteName
     *            RemoteName Name of the file to be deleted, with full path. The
     *            name always starts with a backslash, then the names returned
     *            by FsFindFirst/FsFindNext separated by backslashes.
     * @return Return TRUE if the file could be deleted, FALSE if not.
     *
     * {@inheritDoc}
     */
    public boolean fsDeleteFile(final String remoteName) {
        if (log.isDebugEnabled()) {
            log.debug("TCFacade.fsDeleteFile() remoteName: '" + remoteName + "'");
        }

        try {
            ops.deleteFile(remoteName);
            return true;
        }
        catch (UnsupportedOperationException exc) {
            log.info("UnsupportedOperation - delete path: '" + remoteName + "'");
            return true;
        }
        catch (RuntimeException | IOException exc) {
            log.error(exc.getMessage(), exc);
            return false;
        }
    }

    /** {@inheritDoc} */
    public boolean fsRemoveDir(final String remoteName) {
        if (log.isDebugEnabled()) {
            log.debug("TCFacade.fsRemoveDir() remoteName: '" + remoteName + "'");
        }

        try {
            ops.deleteFile(remoteName);
            return true;
        }
        catch (RuntimeException | IOException exc) {
            log.error(exc.getMessage(), exc);
            return false;
        }
    }

}

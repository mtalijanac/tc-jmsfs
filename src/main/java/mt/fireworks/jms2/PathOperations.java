package mt.fireworks.jms2;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import javax.jms.BytesMessage;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import lombok.Cleanup;
import lombok.extern.log4j.Log4j;
import plugins.FileTime;
import plugins.wfx.WFXPluginInterface;
import plugins.wfx.Win32FindData;

@Log4j
public class PathOperations {


/*
   Path example:
   Within TC: \\\JMSFS\connection - ibmmq.xml\cachingCF\DEV.QUEUE.1\ID:414d5120646576716d202020202020206b659f6017a30040.text
   Within plugin: \connection - ibmmq.xml\cachingCF\DEV.QUEUE.1\ID:414d5120646576716d202020202020206b659f6017a30040.text
 */
    static class Consts {
        final static int idxConf = 0;
        final static int idxCF = 1;
        final static int idxQ = 2;
        final static int idxMsg = 3;


        final static String examples = "_EXAMPLES_";
        final static String path_examples ="\\" + examples;
    }


    ConcurrentHashMap<String, Object> loadedSpringContexes = new ConcurrentHashMap<>();
    Utils utils = new Utils();
    JmsUtils jmsutils = new JmsUtils();


    /** Close the plugin, release all resources. */
    public void unload() {
        log.debug("Closing all contextes");
        for (Object obj: loadedSpringContexes.values()) {
            if (obj instanceof GenericXmlApplicationContext) {
                ((GenericXmlApplicationContext) obj).close();
            }
        }
        loadedSpringContexes.clear();
    }


    /** List the content */
    public ArrayList<Consumer<Win32FindData>> listPath(String fullpath) throws IOException {
        String[] split = StringUtils.split(fullpath, "\\");
        log.info("Opening path: '" + fullpath + "', parsed: '" + Arrays.toString(split) + "'");

        switch(split.length) {
        case 0: return listConfigurationsInWorkingPath(fullpath);
        case 1: return Consts.path_examples.equalsIgnoreCase(fullpath) ?  listExamples(fullpath) : listConnectionsInConfiguration(fullpath);
        case 2: return listQueuesOnConnection(fullpath);
        case 3: return listMessagesInQueues(fullpath);
        }

        log.debug("Returning nothing for path: '" + fullpath + "'");
        return null;
    }


    ArrayList listConfigurationsInWorkingPath(String fullpath) throws IOException {
        File workingDir = utils.workingDir();
        IOFileFilter xmlFilter = FileFilterUtils.suffixFileFilter("xml", IOCase.INSENSITIVE);
        Collection<File> foundXmlFiles = FileUtils.listFiles(workingDir, xmlFilter, null);

        ArrayList results = new ArrayList();

        Consumer<Win32FindData> examplesFolder = utils.fillDisplayData(Consts.examples, Win32FindData.FILE_ATTRIBUTE_DIRECTORY, null, null);
        results.add(examplesFolder);

        for (File f: foundXmlFiles) {
            String path = f.getCanonicalPath();

            String name = f.getName();
            String baseName = FilenameUtils.getBaseName(name);
            Consumer<Win32FindData> configFolder = utils.fillDisplayData(baseName, Win32FindData.FILE_ATTRIBUTE_DIRECTORY, null, null);
            results.add(configFolder);

            long len = f.length();
            long modified = f.lastModified();
            Consumer<Win32FindData> configFile = utils.fillDisplayData(name, Win32FindData.FILE_ATTRIBUTE_NORMAL, len, modified);
            results.add(configFile);
        }

        return results;
    }


    ArrayList listConnectionsInConfiguration(String fullpath) throws IOException {
        GenericXmlApplicationContext ctx = utils.associatedSpringContext(fullpath, loadedSpringContexes);
        Map<String, ConnectionFactory> cfs = ctx.getBeansOfType(ConnectionFactory.class);

        ArrayList results = new ArrayList();
        for (Entry e: cfs.entrySet()) {
            String id = (String) e.getKey();
            ConnectionFactory cf = (ConnectionFactory) e.getValue();

            Consumer<Win32FindData> cfData = utils.fillDisplayData(id, Win32FindData.FILE_ATTRIBUTE_DIRECTORY, null, null);
            results.add(cfData);
        }

        return results;
    }

    ArrayList listExamples(String fullpath) {
        ArrayList results = new ArrayList();

        Consumer<Win32FindData> amq = utils.fillDisplayData("activemq.xml", Win32FindData.FILE_ATTRIBUTE_NORMAL, null, null);
        results.add(amq);

        Consumer<Win32FindData> ibmMq = utils.fillDisplayData("ibmmq.xml", Win32FindData.FILE_ATTRIBUTE_NORMAL, null, null);
        results.add(ibmMq);

        Consumer<Win32FindData> rabbMq = utils.fillDisplayData("rabbitmq.xml", Win32FindData.FILE_ATTRIBUTE_NORMAL, null, null);
        results.add(rabbMq);

        return results;
    }



    ArrayList listQueuesOnConnection(String fullpath) throws IOException {
        GenericXmlApplicationContext ctx = utils.associatedSpringContext(fullpath, loadedSpringContexes);
        Map<String, Queue> qs = ctx.getBeansOfType(Queue.class);
        Map<String, String> qn = ctx.getBeansOfType(String.class);

        HashSet<String> names = new HashSet<String>();
        if (qs != null) names.addAll(qs.keySet());
        if (qn != null) names.addAll(qn.keySet());

        ArrayList results = new ArrayList();
        for (String beanname : names) {
            Consumer<Win32FindData> cfData = utils.fillDisplayData(beanname, Win32FindData.FILE_ATTRIBUTE_DIRECTORY, null, null);
            results.add(cfData);
        }

        return results;
    }


    ArrayList listMessagesInQueues(String fullpath) throws IOException {
        log.debug("Listing messages at path: '" + fullpath + "'");
        String[] split = StringUtils.split(fullpath, "\\");
        String q = split[Consts.idxQ];

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(fullpath, loadedSpringContexes);
        JmsTemplate jt = utils.jmsTemplate(fullpath, loadedSpringContexes);
        Object destination = ctx.getBean(q);

        return jmsutils.browse(jt, destination, (session, qbrowser) -> {
            ArrayList results = new ArrayList();
            Enumeration enumeration = qbrowser.getEnumeration();
            while (enumeration.hasMoreElements()) {
                Message msg = (Message) enumeration.nextElement();
                long jmsTimestamp = msg.getJMSTimestamp();
                String msgfilename = jmsutils.filename(msg);
                Long size = jmsutils.size(msg);

                Consumer<Win32FindData> displayData = utils.fillDisplayData(msgfilename, Win32FindData.FILE_ATTRIBUTE_NORMAL, size, jmsTimestamp);
                results.add(displayData);
            }
            return results;
        });
    }

    public int copyConfiguraton(String sourceFilePath, String destinationPath) throws IOException {
        File source = new File(sourceFilePath);

        if (!source.exists()) {
            log.error("copyConfiguration() - Source file not exists. source: '" + sourceFilePath + "', dest: '" + destinationPath + "'");
            return WFXPluginInterface.FS_FILE_NOTFOUND;
        }

        if (!source.isFile()) {
            log.error("copyConfiguration() - Source is not file. source: '" + sourceFilePath + "', dest: '" + destinationPath + "'");
            return WFXPluginInterface.FS_FILE_NOTSUPPORTED;
        }

        if (!source.canRead()) {
            log.error("copyConfiguration() - Source is readable. source: '" + sourceFilePath + "', dest: '" + destinationPath + "'");
            return WFXPluginInterface.FS_FILE_READERROR;
        }

        if (!FilenameUtils.isExtension(sourceFilePath, "xml")) {
            log.error("copyConfiguration() - Source file not xml configuration. source: '" + sourceFilePath + "', dest: '" + destinationPath + "'");
            return WFXPluginInterface.FS_FILE_READERROR;
        }

        File workingDir = utils.workingDir();
        File dest = new File(workingDir, destinationPath);

        if (source.getCanonicalFile().equals(dest.getCanonicalFile())) {
            log.error("copyConfiguration() - Source and destionate are same file. source: '" + sourceFilePath + "', dest: '" + destinationPath + "'");
            return WFXPluginInterface.FS_FILE_NOTSUPPORTED;
        }

        FileUtils.copyFile(source, dest);
        return WFXPluginInterface.FS_FILE_OK;
    }


    /**
     * Send a local file to a queue
     * @throws IOException
     */
    public void copyLocalFileToQ(String sourceFilePath, String destinationQPath) throws IOException {
        String[] split = StringUtils.split(destinationQPath, "\\");
        String q = split[Consts.idxQ];

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(destinationQPath, loadedSpringContexes);
        JmsTemplate jt = utils.jmsTemplate(destinationQPath, loadedSpringContexes);
        Object queue = ctx.getBean(q);

        File localFile = new File(sourceFilePath);
        boolean isTextFile = FilenameUtils.isExtension(sourceFilePath, new String[] {"txt", "text", "TXT"});

        if (isTextFile) {
            String content = FileUtils.readFileToString(localFile, StandardCharsets.UTF_8);
            jmsutils.send(jt, queue, (session) -> session.createTextMessage(content));
            return;
        }

        byte[] content = FileUtils.readFileToByteArray(localFile);
        jmsutils.send(jt, queue, (session) -> {
            BytesMessage msg = session.createBytesMessage();
            msg.writeBytes(content);
            return msg;
        });
    }


    public Object readMessagePayload(String sourceMsgPath) throws IOException {
        log.debug("readMessagePayload() params: sourceMsgPath: '" + sourceMsgPath + "'");
        String[] split = StringUtils.split(sourceMsgPath, "\\");
        String q = split[Consts.idxQ];

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(sourceMsgPath, loadedSpringContexes);
        JmsTemplate jt = utils.jmsTemplate(sourceMsgPath, loadedSpringContexes);
        Object queue = ctx.getBean(q);

        String messageSelector = utils.messageSelector(sourceMsgPath);
        Message message = jmsutils.receiveSelected(jt, queue, messageSelector);

        if (message == null) {
            return null;
        }

        Object payload = jmsutils.payload(message);
        return payload;
    }


    public Object browsePath(String fullpath) throws IOException {
        String[] split = StringUtils.split(fullpath, "\\");
        log.info("Browsing path: '" + fullpath + "', parsed: '" + Arrays.toString(split) + "'");

        if (fullpath.startsWith(Consts.path_examples)) {
            return browseExampleConfiguration(fullpath);
        }

        switch(split.length) {
        case 1: return browseConfiguration(fullpath);
        default: return browseMessagePayload(fullpath);
        }
    }

    Object browseConfiguration(String confPath) throws IOException {
        File workingDir = utils.workingDir();
        File file = new File(workingDir, confPath);
        if (!file.exists()) {
            log.error("Can't find file: '" + file.getAbsolutePath() + "'");
            return null;
        }

        String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        return content;
    }

    Object browseMessagePayload(String sourceMsgPath) throws IOException {
        log.debug("browseMessagePayload() params: sourceMsgPath: '" + sourceMsgPath + "'");
        String[] split = StringUtils.split(sourceMsgPath, "\\");
        String q = split[Consts.idxQ];

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(sourceMsgPath, loadedSpringContexes);
        JmsTemplate jt = utils.jmsTemplate(sourceMsgPath, loadedSpringContexes);
        Object queue = ctx.getBean(q);

        String messageSelector = utils.messageSelector(sourceMsgPath);
        log.debug("Browsing with selector: [" + messageSelector + "]");

        Message message = jmsutils.browseSelected(jt, queue, messageSelector, (session, browser) -> {
            Enumeration<Message> enumeration = browser.getEnumeration();
            if (!enumeration.hasMoreElements()) {
                return null;
            }
            Message msg = enumeration.nextElement();
            return msg;
        });

        if (message == null) {
            return null;
        }

        Object payload = jmsutils.payload(message);
        return payload;
    }

    Object browseExampleConfiguration(String sourceMsgPath) throws IOException {
        log.debug("browseMessagePayload() params: sourceMsgPath: '" + sourceMsgPath + "'");
        String[] split = StringUtils.split(sourceMsgPath, "\\");
        String filename = split[split.length - 1];

        ClassPathResource cpr = new ClassPathResource("examples/" + filename, this.getClass().getClassLoader());
        @Cleanup InputStream is = cpr.getInputStream();
        byte[] result = IOUtils.toByteArray(is);
        return result;
    }


    public void deleteFile(final String fullpath) throws IOException {
        String[] split = StringUtils.split(fullpath, "\\");
        log.info("Deleting path: '" + fullpath + "', parsed: '" + Arrays.toString(split) + "'");

        switch(split.length) {
        case Consts.idxQ + 1: clearQueue(fullpath); return;
        case Consts.idxMsg + 1: deleteMessage(fullpath); return;
        }
    }

    void clearQueue(String fullpath) throws IOException {
        String[] split = StringUtils.split(fullpath, "\\");
        String q = split[Consts.idxQ];

        log.info("clearQueue path: '" + fullpath + "', parsed: '" + Arrays.toString(split) + "', q: '" + q + "'");

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(fullpath, loadedSpringContexes);
        Object queue = ctx.getBean(q);

        log.debug("Clearing queue: " + queue);

        JmsTemplate jt = utils.jmsTemplate(fullpath, loadedSpringContexes);

        Message msg = null;
        while (true) {
            msg = jmsutils.receiveSelected(jt, queue, null);
            if (msg == null) {
                break;
            }
        }
    }


    Message deleteMessage(final String msgpath) throws IOException {
        String[] split = StringUtils.split(msgpath, "\\");
        String q = split[Consts.idxQ];

        GenericXmlApplicationContext ctx = utils.associatedSpringContext(msgpath, loadedSpringContexes);
        JmsTemplate jt = utils.jmsTemplate(msgpath, loadedSpringContexes);
        Object queue = ctx.getBean(q);

        String messageSelector = utils.messageSelector(msgpath);
        Message message = jmsutils.receiveSelected(jt, queue, messageSelector);
        return message;
    }


    static class Utils {

        File workingDir;

        /** @return aktualni radni folder */
        File workingDir() {
            if (workingDir != null) {
                return workingDir;
            }

            try {
                workingDir = new File(".").getCanonicalFile();
            }
            catch (IOException e) {
                log.error("Unexpected problem calculation canonicalFile", e);
            }
            return workingDir;
        }

        /** Initalize values for Win32FindData struct */
        Consumer<Win32FindData> fillDisplayData(String name, long attributes, Long messageSize, Long creationTime) {
            return data -> {
                data.setFileName(name);
                data.setFileAttributes(attributes);

                if (messageSize != null) {
                    data.setFileSizeHigh(0);
                    data.setFileSizeLow(messageSize);
                }

                if (creationTime != null) {
                    FileTime ft = new FileTime();
                    ft.setDate(creationTime);
                    data.setCreationTime(ft);
                    data.setLastAccessTime(ft);
                }
            };
        }

        GenericXmlApplicationContext associatedSpringContext(String fullpath, ConcurrentHashMap<String, Object> cache) throws IOException {
            String[] split = StringUtils.split(fullpath, "\\");
            String filename = split[Consts.idxConf];
            File workingDir = workingDir();
            File springContextXml = new File(workingDir, filename + ".xml");
            String xmlPath = springContextXml.getCanonicalPath();
            String tsKey = "timestamp: " + xmlPath;
            Long lastModified = springContextXml.lastModified();

            Object cachedContext = cache.get(xmlPath);
            if (cachedContext != null && cachedContext instanceof GenericXmlApplicationContext) {
                GenericXmlApplicationContext context = (GenericXmlApplicationContext) cachedContext;
                Long cachedLastModified = (Long) cache.get(tsKey);
                if (cachedLastModified.equals(lastModified) && context.isActive()) {
                    return context;
                }
            }

            long dur = -System.nanoTime();

            FileSystemResource contextResource = new FileSystemResource( springContextXml );
            GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
            ctx.load(contextResource);
            ctx.refresh();

            cache.put(tsKey, lastModified);
            Object oldCtx = cache.put(xmlPath, ctx);
            if (oldCtx != null) {
                ((GenericXmlApplicationContext) oldCtx).close();
            }

            dur += System.nanoTime();
            log.debug("Loaded context in '" + dur + "' ns.");
            return ctx;
        }

        JmsTemplate jmsTemplate(String path, ConcurrentHashMap<String, Object> cache) throws IOException {
             String[] split = StringUtils.split(path, "\\");
             String connection = split[Consts.idxCF];

             GenericXmlApplicationContext ctx = associatedSpringContext(path, cache);
             ConnectionFactory cf = ctx.getBean(connection, ConnectionFactory.class);

             JmsTemplate jt = new JmsTemplate(cf);
             jt.setReceiveTimeout(100);
             jt.afterPropertiesSet();
             return jt;
        }

        String messageSelector(String path) {
            String[] split = StringUtils.split(path, "\\");
            String msgIdWithType = split[Consts.idxMsg];
            String msgId = StringUtils.split(msgIdWithType, ".")[0];
            String messageSelector = "JMSMessageID='" + msgId + "'";
            return messageSelector;
        }
    }

    static class JmsUtils {
        <T> T browse(JmsTemplate jt, Object destination, BrowserCallback<T> browser) {
            if (destination instanceof String) {
                return jt.browse((String) destination, browser);
            }

            Queue q = (Queue) destination;
            return jt.browse(q, browser);
        }

        <T> T browseSelected(JmsTemplate jt, Object queue, String msgSelector, BrowserCallback<T> browser) {
            if (queue instanceof String) {
                return jt.browseSelected((String) queue, msgSelector, browser);
            }
            return jt.browseSelected((Queue) queue, msgSelector, browser);
        }

        void send(JmsTemplate jt, Object destination, MessageCreator creator) {
            if (destination instanceof String) {
                jt.send((String) destination, creator);
                return;
            }

            Queue q = (Queue) destination;
            jt.send(q, creator);
        }

        Message receiveSelected(JmsTemplate jt, Object destination, String messageSelector) {
            if (destination instanceof String) {
                return jt.receiveSelected((String) destination, messageSelector);
            }
            return jt.receiveSelected((Destination) destination, messageSelector);
        }


        String filename(Message message) {
            try {
                String extension = "";
                if (message instanceof TextMessage) {
                    extension = ".text";
                }
                else if (message instanceof BytesMessage) {
                    extension = ".bytes";
                }
                else if (message instanceof MapMessage) {
                    extension = ".map";
                }
                else if (message instanceof ObjectMessage) {
                    extension = ".object";
                }
                else if (message instanceof StreamMessage) {
                    extension = ".stream";
                }
                String jmsMessageID = message.getJMSMessageID();
                String filename = jmsMessageID + extension;
                return filename;
            }
            catch (JMSException e) {
                log.error("Error reading data from message.", e);
            }
            return "error";
        }

        Long size(Message message) throws JMSException {
            if (message instanceof TextMessage) {
                TextMessage tmsg = (TextMessage) message;
                long textLen = tmsg.getText().length();
                return textLen;
            }
            else if (message instanceof BytesMessage) {
                BytesMessage bmsg = (BytesMessage) message;
                return bmsg.getBodyLength();
            }
            return null;
        }

        Object payload(Message message) {
            if (message instanceof TextMessage) {
                TextMessage tmsg = (TextMessage) message;
                try {
                    String payload = tmsg.getText();
                    return payload;
                }
                catch (JMSException e) {
                    log.error("Error reading text from TextMessage", e);
                    return null;
                }
            }

            if (message instanceof BytesMessage) {
                try {
                    BytesMessage bmsg = (BytesMessage) message;
                    int bodyLength = (int) bmsg.getBodyLength();
                    byte[] data = new byte[bodyLength];
                    bmsg.readBytes(data);
                    return data;
                }
                catch (JMSException e) {
                    log.error("Error reading data from BytesMessage", e);
                    return null;
                }
            }

            log.info("Message type not supported: '" + message.getClass().getSimpleName() + "'.");
            return null;
        }
    }

}

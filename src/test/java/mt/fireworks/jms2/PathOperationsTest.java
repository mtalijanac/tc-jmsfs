package mt.fireworks.jms2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Test;

import plugins.wfx.Win32FindData;

public class PathOperationsTest {


    @Test
    public void testListConfigurationsInWorkingPath() throws Exception {
        PathOperations pathOps = pathOps();

        ArrayList result = pathOps.listConfigurationsInWorkingPath("\\");
        Assert.assertEquals(3, result.size());

        HashMap<String, Win32FindData> results = new HashMap<String, Win32FindData>();
        for (Object o: result) {
            Consumer<Win32FindData> con = (Consumer<Win32FindData>) o;
            Win32FindData data = new Win32FindData();
            con.accept(data);
            String fileName = data.getFileName();
            results.put(fileName, data);
        }

        Assert.assertTrue(results.containsKey("connection"));
        Win32FindData conFolder = results.get("connection");
        Assert.assertEquals(Win32FindData.FILE_ATTRIBUTE_DIRECTORY, conFolder.getFileAttributes());

        Assert.assertTrue(results.containsKey("connection.xml"));
        Win32FindData conFile = results.get("connection.xml");
        Assert.assertEquals(Win32FindData.FILE_ATTRIBUTE_NORMAL, conFile.getFileAttributes());
    }


    @Test
    public void testListConnectionsInConfiguration() throws Exception {
        PathOperations pathOps = pathOps();

        ArrayList result = pathOps.listConnectionsInConfiguration("\\connection");
        Assert.assertEquals(1, result.size());

        Consumer<Win32FindData> con0 = (Consumer<Win32FindData>) result.get(0);
        Win32FindData data1 = new Win32FindData();
        con0.accept(data1);

        Assert.assertEquals("amqConnectionFactory", data1.getFileName());
        Assert.assertEquals(Win32FindData.FILE_ATTRIBUTE_DIRECTORY, data1.getFileAttributes());
    }


    @Test
    public void testListQueuesOnConnection() throws Exception {
        PathOperations pathOps = pathOps();

        ArrayList result = pathOps.listQueuesOnConnection("\\connection\\amqConnectionFactory");
        Assert.assertEquals(2, result.size());


        Consumer<Win32FindData> con0 = (Consumer<Win32FindData>) result.get(0);
        Win32FindData data0 = new Win32FindData();
        con0.accept(data0);

        Assert.assertEquals("Q1", data0.getFileName());
        Assert.assertEquals(Win32FindData.FILE_ATTRIBUTE_DIRECTORY, data0.getFileAttributes());


        Consumer<Win32FindData> con1 = (Consumer<Win32FindData>) result.get(1);
        Win32FindData data1 = new Win32FindData();
        con1.accept(data1);

        Assert.assertEquals("Q2", data1.getFileName());
        Assert.assertEquals(Win32FindData.FILE_ATTRIBUTE_DIRECTORY, data1.getFileAttributes());
    }


    PathOperations pathOps() throws IOException {
        PathOperations pathOps = new PathOperations();
        pathOps.utils.workingDir = new File("resources").getCanonicalFile();
        return pathOps;
    }
}

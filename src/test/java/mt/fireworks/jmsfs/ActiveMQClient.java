package mt.fireworks.jmsfs;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class ActiveMQClient {

    public static void main(String[] args) throws JMSException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String now = sdf.format(new Date());
        String brokerUri = "tcp://localhost:61616?jms.useAsyncSend=true";
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(brokerUri);
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);

        jmsTemplate.send("Q1", new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage("Message at: " + now);
            }
        });
    }

}
